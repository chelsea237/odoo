from odoo import models, api, fields, _
from lxml import etree

class RapportApprov(models.TransientModel):
    _name = "rapport.approvisionnement"
    _description = "Rapport approvisionnement"

    date_start = fields.Date(string='Date de debut')
    date_end = fields.Date(string='Date de fin')

    def report_submit(self):
        data = {

            'model_id': self.id,
            "model": "sale.order",
            "form": self.read(["date_start", "date_end"])[0],

        }
        return self.env.ref("dps_report.rapport_approv_template_id").report_action(None, data=data)





