from odoo import models, api, fields, _
from lxml import etree

class RapportVenteWizard(models.TransientModel):
    _name = "rapport.vente.wizard"
    _description = "Rapport vente"

    date_start = fields.Date(string='Date de debut')
    date_end = fields.Date(string='Date de fin')

    def report_submit(self):
        data = {

            'model_id': self.id,
            "model": "pos.order",
            "form": self.read(["date_start", "date_end"])[0],

        }
        return self.env.ref("dps_report.rapport_vente_template_id").report_action(None, data=data)



    # def report_submit(self):
    #     data = {
    #         'model_id': self.id,
    #         "ids": self.ids,
    #         "model": "sale.order",
    #         "form": self.read(["date_start", "date_end"])[0],
    #     }
    #     return self.env.ref("dps_report.rapport_vente_template_id").report_action(
    #         self, data=data
    #     )

