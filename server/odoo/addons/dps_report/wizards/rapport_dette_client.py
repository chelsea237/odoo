from odoo import models, api, fields, _
from lxml import etree

class RapportDetteWizard(models.TransientModel):
    _name = "rapport.dette.wizard"
    _description = "Rapport dette"

    date_start = fields.Date(string='Date de debut')
    date_end = fields.Date(string='Date de fin')

    def report_submit(self):
        data = {

            'model_id': self.id,
            "model": "pos.order",
            "form": self.read(["date_start", "date_end"])[0],

        }
        return self.env.ref("dps_report.rapport_creance_client_id").report_action(None, data=data)





