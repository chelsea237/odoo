# -*- coding: utf-8 -*-

from odoo import api, models, fields
import time
from datetime import datetime
from dateutil.relativedelta import relativedelta




class ReportTresorerie(models.AbstractModel):
    _name = 'report.dps_report.rapport_tresorerie_view'


    def _get_folio_data(self, date_start, date_end):
        total_amount = 0.0
        data_folio = []
        po_obj = self.env["account.move"]
        po_line_obj = self.env["account.move.line"]
        act_domain = [
            ("invoice_date", ">=", date_start),
            ("invoice_date", "<=", date_end),


        ]
        po_ids = po_obj.search(act_domain)
        po_ids = [x.id for x in po_ids]

        pol_ids = po_line_obj.search([('invoice_line_ids', 'in', po_ids)])


        for data in pol_ids:
            data_folio.append(
                {
                    "name": data.name,


                }
            )




        return data_folio


    @api.model
    def _get_report_values(self, docids, data):
        # self.model = self.env.context.get("active_model")
        if data is None:
            data = {}
        if not docids:
            docids = data["form"].get("docids")
        folio_profile = self.env["sale.order"].browse(docids)
        date_start = data["form"].get("date_start", fields.Date.today())
        date_end = data["form"].get(
            "date_end",
            str(datetime.now() + relativedelta(months=+1, day=1, days=-1))[
            :10
            ],
        )

        return {
            "doc_ids": docids,
            # "doc_model": self.id,
            "data": data["form"],
            "docs": folio_profile,
            "time": time,
            "folio_data": self._get_folio_data(date_start, date_end),
        }








