# -*- coding: utf-8 -*-

from odoo import api, models, fields
import time
from datetime import datetime
from dateutil.relativedelta import relativedelta




class ReportSales(models.AbstractModel):
    _name = 'report.dps_report.vente_report_view'


    def _get_folio_data(self, date_start, date_end):
        total_amount = 0.0
        data_folio = []
        po_obj = self.env["pos.order"]
        po_line_obj = self.env["pos.order.line"]
        act_domain = [
            ("date_order", ">=", date_start),
            ("date_order", "<=", date_end),
            ("state", "=", "invoiced"),

        ]
        po_ids = po_obj.search(act_domain)
        po_ids = [x.id for x in po_ids]

        pol_ids = po_line_obj.search([('order_id', 'in', po_ids)])

        total = 0.0
        total_qte = 0.0
        total_prix = 0.0
        total_mb = 0.0
        vente_comptant = 0.0
        vente_credit = 0.0
        total_vente = 0.0
        total_cout = 0.0
        for data in pol_ids:
            data_folio.append(
                {
                    "name": data.product_id.default_code,
                    "date": data.order_id.date_order,
                    "description": data.product_id.name,
                    "quantite": data.qty,
                    "prix": data.price_unit,
                    "montant": data.price_subtotal,
                    "cout": data.product_id.standard_price * data.qty,
                    "mb" : data.price_subtotal - (data.product_id.standard_price * data.qty),
                    "pourcentage": ((data.price_subtotal - data.product_id.standard_price) / data.price_subtotal) * 100

                }
            )

            total += data.price_subtotal
            total_qte += data.qty
            total_prix += data.price_unit
            total_mb += data.price_subtotal - data.product_id.standard_price
            total_cout += data.product_id.standard_price * data.qty
            for line in data.order_id.payment_ids.payment_method_id:
                if line.name == "Compte client" and data.order_id.account_move.payment_state == "paid" or data.order_id.account_move.payment_state == "partial":
                    vente_credit += data.price_subtotal
                else:
                    vente_comptant += data.price_subtotal
            # vente_credit = vente_credit
            # vente_comptant = vente_comptant
            total_vente = vente_comptant + vente_credit

            print("test1",data.order_id.account_move.payment_state )
            print("test2", data.order_id.account_move.id)
            # print("test3", total_vente)


        data_folio.append({"total_amount": total, "total_qte": total_qte, "total_prix":total_prix, "total_mb":total_mb,
                           "total_cout":total_cout, "vente_credit":vente_credit, "vente_comptant":vente_comptant,
                           "total_vente":total_vente})

        return data_folio


    @api.model
    def _get_report_values(self, docids, data):
        # self.model = self.env.context.get("active_model")
        if data is None:
            data = {}
        if not docids:
            docids = data["form"].get("docids")
        folio_profile = self.env["sale.order"].browse(docids)
        date_start = data["form"].get("date_start", fields.Date.today())
        date_end = data["form"].get(
            "date_end",
            str(datetime.now() + relativedelta(months=+1, day=1, days=-1))[
            :10
            ],
        )

        return {
            "doc_ids": docids,
            # "doc_model": self.id,
            "data": data["form"],
            "docs": folio_profile,
            "time": time,
            "folio_data": self._get_folio_data(date_start, date_end),
        }








