# -*- coding: utf-8 -*-

from odoo import api, models, fields
import time
from datetime import datetime
from dateutil.relativedelta import relativedelta




class ReportSales(models.AbstractModel):
    _name = 'report.dps_report.approv_report_view'

    def _get_folio_data(self, date_start, date_end):
        total_amount = 0.0
        data_folio = []
        po_obj = self.env["stock.picking"]
        po_line_obj = self.env["stock.move.line"]
        act_domain = [
            ("date_done", ">=", date_start),
            ("date_done", "<=", date_end),
            # ("state", "=", "done")
        ]
        po_ids = po_obj.search(act_domain)

        # total = 0.0
        total_qte = 0.0
        for data in po_ids:
            data_folio.append(
                {
                    "name": data.picking_id.name,
                    "date": data.picking_id.date_done,
                    "quantite": data.qty_done,

                }
            )
            # total += data.price_unit * data.product_qty
            total_qte += data.qty_done
        # data_folio.append({"total_amount": total})
        data_folio.append({"total_qte": total_qte})
        return data_folio


    @api.model
    def _get_report_values(self, docids, data):
        # self.model = self.env.context.get("active_model")
        if data is None:
            data = {}
        if not docids:
            docids = data["form"].get("docids")
        folio_profile = self.env["stock.picking"].browse(docids)
        date_start = data["form"].get("date_start", fields.Date.today())
        date_end = data["form"].get(
            "date_end",
            str(datetime.now() + relativedelta(months=+1, day=1, days=-1))[
            :10
            ],
        )
        return {
            "doc_ids": docids,
            # "doc_model": self.model,
            "data": data["form"],
            "docs": folio_profile,
            "time": time,
            "folio_data": self._get_folio_data(date_start, date_end),
        }