# -*- coding: utf-8 -*-
{
    'name': "reporting DPS",

    'summary': """reporting""",

    'description': """
      
    """,

    'author': "TISS",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'report',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'sale', 'stock','point_of_sale', 'purchase'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'wizards/rapport_vente.xml',
        'views/rapport_vente_view.xml',
        'wizards/etat_approvisionnement.xml',
        'wizards/report_tresorerie_wizard.xml',
        'wizards/rapport_dette_client.xml',
        'views/etat_approv_view.xml',
        'views/rapport_tresorerie.xml',
        'views/rapport_creance_client.xml',
        'views/rapport_charge.xml',
        'views/rapport_salaire.xml',
        'views/etat_dette_engagement.xml',
        'views/etat_stock_boutique.xml',

    ],
    # only loaded in demonstration mode
    'demo': [
        # 'demo/demo.xml',
    ],
}
